import { 
  BellIcon,
  CalendarIcon,
  UserIcon,
  DocumentDuplicateIcon,
  HomeIcon,
  HashtagIcon,
 } from '@heroicons/react/24/outline'

export default function Home() {
  return (
    <div className="flex min-w-full h-full flex-col border-b border-gray-200 bg-white">
      <header className="shrink-0 border-b border-gray-200 bg-white">
        <div className="mx-auto flex h-16 max-w-7xl items-center justify-between px-4 sm:px-6 lg:px-8 text-purple-600">
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 6.042A8.967 8.967 0 006 3.75c-1.052 0-2.062.18-3 .512v14.25A8.987 8.987 0 016 18c2.305 0 4.408.867 6 2.292m0-14.25a8.966 8.966 0 016-2.292c1.052 0 2.062.18 3 .512v14.25A8.987 8.987 0 0018 18a8.967 8.967 0 00-6 2.292m0-14.25v14.25" />
          </svg>

          <div className="flex items-center gap-x-8">
            <button type="button" className="-m-2.5 p-2.5 text-gray-400 hover:text-purple-600 hover:bg-gray-50">
              <span className="sr-only">View notifications</span>
              <BellIcon className="h-6 w-6" aria-hidden="true" />
            </button>
            <a href="#" className="-m-1.5 p-1.5">
              <span className="sr-only">Your profile</span>
              <img
                className="h-8 w-8 rounded-full bg-gray-800"
                src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                alt=""
              />
            </a>
          </div>
        </div>
      </header>

      <div className="mx-auto flex w-full max-w-7xl items-start gap-x-8 px-4 py-10 sm:px-6 lg:px-8">
        <aside className="sticky top-8 hidden w-44 shrink-0 lg:block">
          <LeftAside />  
        </aside>

        <main className="flex-1">
          <MainArea />  
        </main>

        <aside className="sticky top-8 hidden w-96 shrink-0 xl:block">
          <RightAside />
        </aside>
      </div>
    </div>
  )
}


const navigation = [
  { name: 'Home', href: '/home', icon: HomeIcon, count: '5', current: true },
  { name: 'Explore', href: '/explore', icon: HashtagIcon, current: false },
  { name: 'Notifications', href: '/notifications', icon: BellIcon, count: '12', current: false },
  { name: 'Messages', href: '/messages', icon: CalendarIcon, count: '20+', current: false },
  { name: 'Bookmarks', href: '/bookmarks', icon: DocumentDuplicateIcon, current: false },
  { name: 'Profile', href: '/profile', icon: UserIcon, current: false },
]

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

const LeftAside = () => {
  return (
    <nav className="w-69 flex flex-1 flex-col border-b border-gray-200 bg-white" aria-label="Sidebar">
      <ul role="list" className="-mx-2 space-y-1 border-b border-gray-200 bg-white">
        {navigation.map((item) => (
          <li key={item.name}>
            <a
              href={item.href}
              className={classNames(
                item.current ? 'bg-gray-50 text-purple-600' : 'text-gray-700 hover:text-purple-600 hover:bg-gray-50',
                'group flex gap-x-3 rounded-md p-2 leading-6 font-semibold text-xl'
              )}
            >
              <item.icon
                className={classNames(
                  item.current ? 'text-purple-600' : 'text-gray-400 group-hover:text-purple-600',
                  'h-6 w-6 shrink-0'
                )}
                aria-hidden="true"
              />
              {item.name}
              {item.count ? (
                <span
                  className="ml-auto w-9 min-w-max whitespace-nowrap rounded-full bg-white px-2.5 py-0.5 text-center text-xs font-medium leading-5 text-gray-600 ring-1 ring-inset ring-gray-200"
                  aria-hidden="true"
                >
                  {item.count}
                </span>
              ) : null}
            </a>
          </li>
        ))}
        <li xey="btnPost" className="group flex gap-x-3 rounded-md p-2 leading-6 font-semibold text-xl">
          <button
            type="button"
            className="w-full rounded-3xl bg-purple-600 px-4 py-2.5 font-semibold text-white shadow-sm hover:bg-purple-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-purple-600"
          >
            My 2 cents
          </button>
        </li>
      </ul>
    </nav>
  )
}


const RightAside = () => {
  return <p>Right Column Area</p>
}

const MainArea = () => {
  return <p>Main Area</p>
}
